#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#pragma once

#include <QtSql>
#include <QFileInfo>

class databaseManager
{
public:

    /// <summary>
    /// Connects to Database
    /// </summary>
    /// <returns>1 if connected, otherwise 0</returns>
    static int connect()
    {

        QSqlDatabase db;
        if(QSqlDatabase::contains("qt_sql_default_connection")) {

            db = QSqlDatabase::database("qt_sql_default_connection");
        }
        else {
            db = QSqlDatabase::addDatabase("QSQLITE");
        }

        db.setHostName("localhost");
        db.setUserName("Root");
        db.setPassword("");
       // const QString DBPath = QFileInfo(".").absolutePath() + "/Book_Management_System/dataBase/usersData.db";

        //db.setDatabaseName(DBPath);

        db.setDatabaseName("C:/Users/Amero/Documents/projekts/qt/Book_Management_System/Book_Management_System/dataBase/SQLite-Database.db");

        if (db.open())
        {
            return 1;
        }
        return 0;
    }

    /// <summary>
    /// Closes the DB Connection.
    /// </summary>
    static void close()
    {
        {
            QSqlDatabase db = QSqlDatabase::database();
            db.close();
        }
        QSqlDatabase::removeDatabase(QSqlDatabase::defaultConnection);
    }
    int add_new_order(QString ordernumber, QString user_name, QString book_name, QString date_toback,QString back_Date)
    {
        QSqlQuery query;
        //query.prepare("INSERT INTO rentorder (order_number,user_id, name_book, backdate,back_Date)"
         //           "VALUES (:ordernumber,:user_name, :book_name, :date_toback, :back_Date)");
        query.prepare("INSERT INTO rentorder ("
            "order_number,"
            "user_id,"
            "name_book,"
            "backdate,"
            "back_Date) "
            "VALUES (?,?,?,?,?);");
        query.addBindValue(ordernumber);
        query.addBindValue(user_name);
        query.addBindValue(book_name);
        query.addBindValue(date_toback);
        query.addBindValue(back_Date);
        if (!query.exec())
        {
            return 0;
        }
        return 1;
    }
    int delete_order(QString id)
        {
            QSqlQuery query;
            query.prepare("DELETE FROM rentorder WHERE order_number=(?)");
            query.addBindValue(id);
            if (!query.exec())
            {
                return query.numRowsAffected();
            }
            return query.numRowsAffected();
        }
    // ---------------- book queries ----------------

    /// <summary>
    /// Adds a new book to the database
    /// </summary>
    /// <param name="serialnumber"></param>
    /// <param name="name"></param>
    /// <param name="owner"></param>
    /// <param name="created_Date"></param>
    /// <param name="model"></param>
    /// <param name="number_of_books"></param>
    /// <returns></returns>
    int add_book(QString serialnumber, QString name, QString author, QString publication_date, QString genre,QString amount_in_stock)
    {
        QSqlQuery query;
        query.prepare("INSERT INTO books ("
            "serialnumber,"
            "name,"
            "author,"
            "publication_date,"
            "genre,"
            "amount_in_stock) "
            "VALUES (?,?,?,?,?,?);");
        query.addBindValue(serialnumber);
        query.addBindValue(name);
        query.addBindValue(author);
        query.addBindValue(publication_date);
        query.addBindValue(genre);
        query.addBindValue(amount_in_stock);
        if (!query.exec())
        {
            return 0;
        }
        return 1;
    }

    /// <summary>
    /// updates the information of a book based on its ID
    /// </summary>
    /// <param name="serialnumber"></param>
    /// <param name="name"></param>
    /// <param name="owner"></param>
    /// <param name="created_Date"></param>
    /// <param name="model"></param>
    /// <param name="number_of_books"></param>
    /// <param name="user_id"></param>
    /// <returns></returns>
    int update_book(QString serialnumber, QString name, QString author, QString publication_date, QString genre, QString amount_in_stock, QString user_id)
    {
        QSqlQuery query;
        query.prepare("UPDATE books SET name=(?), owner=(?), created_Date=(?), model=(?) WHERE serialnumber=(?)");
        query.addBindValue(name);
        query.addBindValue(author);
        query.addBindValue(publication_date);
        query.addBindValue(genre);
        query.addBindValue(amount_in_stock);
        query.addBindValue(user_id);
        query.addBindValue(serialnumber);
        if (!query.exec())
        {
            return 0;
        }
        return 1;
    }


    int update_value_book(QString val, QString id)
    {
        QSqlQuery query;
        query.prepare("UPDATE books SET amount_in_stock=(?) WHERE serialnumber=(?)");
        query.addBindValue(val);
        query.addBindValue(id);
        if (!query.exec())
        {
            return 0;
        }
        return 1;
    }
    /// <summary>
    /// deletes a book based on its ID
    /// </summary>
    /// <param name="serialnumber"></param>
    /// <returns></returns>
    int delete_book(QString serialnumber)
    {
        QSqlQuery query;
        query.prepare("DELETE FROM Books WHERE serialnumber=(?)");
        query.addBindValue(serialnumber);
        if (!query.exec())
        {
            return query.numRowsAffected();
        }
        return query.numRowsAffected();
    }

    /// <summary>
    /// checks whether the book with the given ID  exist.
    /// </summary>
    /// <param name="serialnumber"></param>
    /// <returns></returns>
    int check_id(QString id)
    {
        QSqlQuery query;
        query.prepare("SELECT 1 FROM books WHERE serialnumber=(?) LIMIT 1");
        query.addBindValue(id);
        if (!query.exec())
        {
            return 1;
        }
        return 0;
    }

    /// <summary>
    /// load all books
    /// </summary>
    /// <returns></returns>
    QSqlQueryModel* load_all_books()
    {


        QSqlQuery query;
        query.prepare("Select books.serialnumber as ID, books.name as Name, books.author as Author, books.publication_date as date, books.genre as Genre  From books");
        query.exec();
        QSqlQueryModel* model = new QSqlQueryModel();
        model->setQuery(query);
        //return  query;
        return model;
    }

    /// <summary>
    /// load all reserved books with user ID
    /// </summary>
    /// <param name="user_ID"></param>
    /// <returns></returns>
    QSqlQuery load_reserved_books(QString user_ID)
    {
        QSqlQuery query;
        query.prepare("Select * From books where books.user_id =(?)");
        query.addBindValue(user_ID);
        query.exec();
        return  query;
    }

    /// <summary>
    /// load a book with an ID
    /// </summary>
    /// <returns></returns>
    QSqlQuery load_book_ID()
    {
        QSqlQuery query;
        query.prepare("Select books.serialnummer as Book From books");
        query.exec();
        return  query;
    }

    // ---------------- user queries ----------------

    /// <summary>
    /// add a new user
    /// </summary>
    /// <param name="user_id"></param>
    /// <param name="name"></param>
    /// <param name="email"></param>
    /// <param name="number"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    QSqlQuery add_user(QString user_id, QString name, QString email, QString number, QString password)
    {
        QSqlQuery query;
        /*query.prepare("INSERT INTO user ("
            "user_id,"
            "name,"
            "email,"
            "number,"
            "password) "
            "VALUES (?,?,?,?,?);");*/
        query.prepare("INSERT INTO user (user_id,name, email, number, password)"
                    "VALUES (:user_id,:name, :email, :number, :password)");
        query.addBindValue(user_id);
        query.addBindValue(name);
        query.addBindValue(email);
        query.addBindValue(number);
        query.addBindValue(password);
        return query;
    }
    QSqlQuery login_user(QString userName, QString password)
    {
        qDebug()<<"connection succeed";
        //QSqlQuery query(QSqlDatabase::database("QSQLITE"));
        QSqlQuery query;
        query.prepare("Select * from user Where name ='" + userName +"' and password= '"+password +"'");
        // retrived data from database
        //query.bindValue(":userName", userName);
        //query.bindValue(":password", password);
       return query;

    }
    /// <summary>
    /// update a user information based on his id
    /// </summary>
    /// <param name="user_id"></param>
    /// <param name="name"></param>
    /// <param name="email"></param>
    /// <param name="number"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    int update_user(QString user_id, QString name, QString email, QString number, QString password)
    {
        QSqlQuery query;
        query.prepare("UPDATE user SET name=(?), email=(?), number=(?), password=(?) WHERE user_id=(?)");
        query.addBindValue(name);
        query.addBindValue(email);
        query.addBindValue(number);
        query.addBindValue(password);
        query.addBindValue(user_id);
        if (!query.exec())
        {
            return 0;
        }
        return 1;
    }

    /// <summary>
    /// delete a user based on his id
    /// </summary>
    /// <param name="user_id"></param>
    /// <returns></returns>
    int delete_user(QString user_id)
    {
        QSqlQuery query;
        query.prepare("DELETE FROM user WHERE user_id=(?)");
        query.addBindValue(user_id);
        if (!query.exec())
        {
            return query.numRowsAffected();
        }
        return query.numRowsAffected();
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="user_id"></param>
    /// <returns></returns>
    QSqlQueryModel* search_user(QString user_id)
    {
        QSqlQueryModel* model = new QSqlQueryModel();
        QSqlQuery query;
        query.prepare("Select user.user_id FROM user WHERE user_id=(?)");
        query.addBindValue(user_id);
        query.exec();
        model->setQuery(query);
        return model;
    }
    /// <summary>
    /// get all users
    /// </summary>
    /// <param name="user_id"></param>
    /// <returns></returns>
    QSqlQueryModel* load_all_users(QString user_id)
    {
        QSqlQueryModel* model = new QSqlQueryModel();
        QSqlQuery query;
        query.prepare("SELECT user.user_id FROM user WHERE user_id=(?)");
        query.addBindValue(user_id);
        query.exec();
        model->setQuery(query);
        //return  query;
        return model;
    }




}
;

#endif // DATABASEMANAGER_H

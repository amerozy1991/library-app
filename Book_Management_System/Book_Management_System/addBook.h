#ifndef ADDBOOK_H
#define ADDBOOK_H

#include <QDialog>
#include <QDebug>
#include <QMessageBox>
#include <QPainter>
#include <QString>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlQueryModel>
#include "databaseManager.h"
namespace Ui {
class addBook;
}

class addBook : public QDialog
{
    Q_OBJECT

public:
    explicit addBook(QWidget *parent = nullptr);
    ~addBook();

    void paintEvent(QPaintEvent *);
    void clearInput();

private slots:
    void on_submitBtn_clicked();
    void getInput();
private:
    Ui::addBook *ui;

    QSqlQuery sql;

    QString book_name;
    QString book_author;
    QString book_publication_date;
    QString book_genre;
    QString book_amount_in_stock;
    databaseManager db;
    QString _id;
};

#endif // ADDBOOK_H

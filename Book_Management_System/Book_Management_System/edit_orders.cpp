#include "edit_orders.h"
#include "ui_edit_orders.h"



edit_orders::edit_orders(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::edit_orders)
{
    ui->setupUi(this);
    this->setWindowTitle("Edit an order");
    connect(ui->searchArea, SIGNAL(textChanged(QString)),this, SLOT(searchFunc()));
    db.connect();
    appendData();
    refreshData();

}

edit_orders::~edit_orders()
{
    delete ui;
}
void edit_orders::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPixmap pix;
    pix.load(":/images/rose2.png");
    painter.drawPixmap(0,0,pix);

}

void edit_orders::appendData()
{
    db.connect();
    sqlModel = new QSqlQueryModel();
    sqlModel->setQuery("select order_number, user_id, name_book, backdate from rentorder");
    ui->tableView->verticalHeader()->setVisible(false);   //hide header
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView->setModel(sqlModel);
}
QStringList edit_orders::getCurrentData()
{
    db.connect();
    QStringList rowData;
    //get current row data
    int row = ui->tableView->currentIndex().row();
    QAbstractItemModel *model = ui->tableView->model();
    //QModelIndex index0 = model->index(row, 0);  //two ways to get index

    if(row != -1)
    {
        //get the value of the whole row that user chosed
        rowData << model->index(row, 0).data().toString();
        rowData << model->index(row, 1).data().toString();
        rowData << model->index(row, 2).data().toString();
        rowData << model->index(row, 3).data().toString();
        rowData << model->index(row, 4).data().toString();
    }
    return rowData;
}
void edit_orders::searchFunc()
{
    db.connect();
    QSqlQueryModel *model=new QSqlQueryModel;
    QString search,
            data;
    search = ui->searchArea->text();
    data = "select order_number, user_id, name_book, backdate from rentorder where order_number LIKE '%"+search+"%'";
    model->setQuery(data);
    ui->tableView->setModel(model);
}
void edit_orders::deleteorder()
{

    db.connect();
    QStringList rowData;
    rowData = this->getCurrentData();
    QString val;
    val= (rowData.at(0));
    db.delete_order(val);
    QMessageBox::information(this, "-", "deleted");
    db.close();
}
void edit_orders::on_DeleteBtn_clicked()
{
    this->deleteorder();
    appendData();

}

void edit_orders::on_btn_checkorder_clicked()
{
    db.connect();

    QStringList id_of_user;
    id_of_user = this->getCurrentData();
    QString val_id_of_user;
    val_id_of_user= (id_of_user.at(1));
    QSqlQuery sql_useremail;
    sql_useremail.prepare("SELECT email FROM user WHERE user_id = '"+ val_id_of_user +"' ");
    sql_useremail.exec();
    QString useremail;
    if (sql_useremail.next())
    {
        useremail=sql_useremail.value(0).toString();
        qDebug() <<"usr email is" <<useremail;
    }
    userMail = useremail;
    db.close();

    QDateTime date_check = QDateTime::currentDateTime();
    QString formattedTime_check = date_check.toString("dd.MM.yyyy hh:mm:ss");
    QByteArray formattedTimeMsg_check = formattedTime_check.toLocal8Bit();

 qDebug() <<"usr email is" <<useremail;


}


void edit_orders::on_pushButton_clicked()
{


}

void edit_orders::refreshData()
{
    //remove row by row
    QAbstractItemModel *model = ui->tableView->model();
    int rowCount = model->rowCount();
    for(int r = rowCount - 1; r >= 0; r--) {
        model->removeRow(r);
    }

    appendData();

}

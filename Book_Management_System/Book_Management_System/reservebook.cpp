#include "reservebook.h"
#include "ui_reservebook.h"



reservebook::reservebook(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::reservebook)
{
    ui->setupUi(this);
    this->setFixedSize(500, 600);   //fix screen size
    this->setWindowTitle("Reserve book");


    connect(ui->confirmBtn, &clickableLabel::clicked, [=](){
        confirm_Button_On();
        this->close();
    });
}

reservebook::~reservebook()
{
    delete ui;
}
void reservebook::setReserveData(QStringList rowData)
{
    ui->txtbookName->setText(rowData.at(1));
    ui->txt_owner->setText(rowData.at(2));
    ui->txt_created_Date->setText(rowData.at(3) + "L");
    ui->txt_model->setText(rowData.at(4));
    ui->txt_number_in_store->setText(rowData.at(5));
}

void reservebook::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPixmap pix;
    pix.load(":/images/white.png");
    painter.drawPixmap(0,0,pix);

}

void reservebook::confirm_Button_On()
{
    db.connect();
    if(db.connect()==1) {
        qDebug() <<"database connect succeed reserve";
        //db.add_new_order("12","inputName","bookname","formattedTime");

        qDebug() <<inputName;
        QString orderNumber = generateOrderNumber(6);
        QMessageBox::information(this, "Order Number", "Thank you! "
                            "Your order number is: " + orderNumber);
        QString books_in_store;
        QSqlQuery sql2;
        QString newnumber;
        int newnum = 0;
        QString temp2 =ui->txtbookName->text();
        sql2.prepare("SELECT amount_in_stock FROM books WHERE name = '"+ temp2 +"' ");
        sql2.exec();
        if (sql2.next())
        {
            int n=sql2.value(0).toInt();
            qDebug()<<n;
            newnum = n-1;
            qDebug()<<newnum;
            newnumber.setNum(newnum);
        }
        QSqlQuery sql_userid;
        sql_userid.prepare("SELECT user_id FROM user WHERE name = '"+ inputName +"' ");
        sql_userid.exec();
        QString iduser;
        if (sql_userid.next())
        {
            iduser=sql_userid.value(0).toString();
            qDebug() <<"id user is" <<iduser;
        }
        if(newnum >= 0)
        {
            books_in_store = QString("Update books set amount_in_stock = '"+newnumber+"' Where name = '"+ui->txtbookName->text()+"' ");
            QSqlQuery sql;
            QString bookname = ui->txtbookName->text();
            QDateTime date = QDateTime::currentDateTime();
            QString formattedTime = date.toString("dd.MM.yyyy hh:mm:ss");
            QByteArray formattedTimeMsg = formattedTime.toLocal8Bit();

            QDateTime date2 = date.addDays(14);
            QString formattedTime2 = date2.toString("dd.MM.yyyy hh:mm:ss");
            QByteArray formattedTimeMsg2= formattedTime2.toLocal8Bit();

            db.add_new_order(orderNumber,iduser,bookname,formattedTime,formattedTime2);

            if(sql.exec(books_in_store) )
            {
                qDebug() <<"Update successed";
            }
            else
            {
                qDebug() <<"failed to get Order Number";
                qDebug() <<sql.lastError();
            }
        }
        else
        {
            QMessageBox::information(this, "Order Number", "Thank you! "
                                                        "but this book is not available ");
        }
    }
}
QString reservebook::generateOrderNumber(int length)
{
    qsrand(QDateTime::currentMSecsSinceEpoch());
    const char chrs[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    int chrs_size = sizeof(chrs);

    char* ch = new char[length + 1];
    memset(ch, 0, length + 1);
    int randomx = 0;
    for (int i = 0; i < length; ++i)
    {
        randomx= rand() % (chrs_size - 1);
        ch[i] = chrs[randomx];
    }
    QString ret(ch);
    delete[] ch;
    return ret;
}

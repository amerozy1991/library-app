#ifndef FINDBOOKPAGE_H
#define FINDBOOKPAGE_H

#include <QWidget>
#include <QPainter>
#include <QtSql>
#include <QSqlDatabase>
#include <QMessageBox>
#include <QDebug>
#include <QAbstractItemModel>
#include"reservebook.h"
#include <QString>
#include "databaseManager.h"
namespace Ui {
class findbookPage;
}

class findbookPage : public QWidget
{
    Q_OBJECT

public:
    explicit findbookPage(QWidget *parent = nullptr);
    ~findbookPage();

    void paintEvent(QPaintEvent *);

    void connectData();
    void appendData();
    void refreshData();
    QStringList getCurrentData();
    QString inputName;

signals:
    void backToMainPage();

private slots:
    void on_backBtn_clicked();

    void on_reserveBtn_clicked();

    void searchFunc();

private:
    Ui::findbookPage *ui;

    QSqlQueryModel *sqlModel;
    QSqlDatabase database;
    databaseManager db;
    reservebook* reservePage;
};

#endif // FINDBOOKPAGE_H

#ifndef RESERVEBOOK_H
#define RESERVEBOOK_H

#include <QDialog>
#include <QStringList>
#include <QPainter>
#include <QMessageBox>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>
#include <QSqlDatabase>
#include <QDateTime>
#include <QtGlobal>
#include "databaseManager.h"
namespace Ui {
class reservebook;
}

class reservebook : public QDialog
{
    Q_OBJECT

public:
    explicit reservebook(QWidget *parent = nullptr);
    ~reservebook();
    void setReserveData(QStringList rowData);
        void paintEvent(QPaintEvent *);
        void confirm_Button_On();

        QString inputName;
        QString generateOrderNumber(int length);
private:
    Ui::reservebook *ui;
    databaseManager db;
};

#endif // RESERVEBOOK_H

#include "findbookpage.h"
#include "ui_findbookPage.h"

findbookPage::findbookPage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::findbookPage)
{
    ui->setupUi(this);
    this->setFixedSize(1100, 700);
        this->setWindowTitle("Find Book");
        connect(ui->searchArea, SIGNAL(textChanged(QString)),this, SLOT(searchFunc()));
        connectData();
        appendData();
        refreshData();
}

findbookPage::~findbookPage()
{
     database.close();
    delete ui;
}
void findbookPage::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPixmap pix;
    pix.load(":/images/Mystic.png");
    painter.drawPixmap(0,0,pix);
}

void findbookPage::appendData()
{
    ui->tableView->verticalHeader()->setVisible(false);   //hide header
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView->setModel(sqlModel);
}


QStringList findbookPage::getCurrentData()
{
    QStringList rowData;
    //get current row data
    int row = ui->tableView->currentIndex().row();
    QAbstractItemModel *model = ui->tableView->model();
    //QModelIndex index0 = model->index(row, 0);  //two ways to get index

    if(row != -1)
    {
        //get the value of the whole row that user chosed
        rowData << model->index(row, 0).data().toString();
        rowData << model->index(row, 1).data().toString();
        rowData << model->index(row, 2).data().toString();
        rowData << model->index(row, 3).data().toString();
        rowData << model->index(row, 4).data().toString();
        rowData << model->index(row, 5).data().toString();
       //rowData << model->data(index0).toString(); as another way
    }
    return rowData;
}


void findbookPage::on_backBtn_clicked()
{
    emit this->backToMainPage();

}

void findbookPage::on_reserveBtn_clicked()
{
    reservePage = new reservebook;
    //use input name to match database
    reservePage->inputName = inputName;
    QStringList rowData;
    rowData = this->getCurrentData();

    if(rowData.isEmpty()) {
        QMessageBox::information(this, tr("Alert") , tr("Please choose a book!"));
        return;
    }
    else {
        reservePage->setReserveData(rowData);
    }

    qDebug() << getCurrentData();
    reservePage->activateWindow();
   reservePage->exec();
}

void findbookPage::searchFunc()
{
    QSqlQueryModel *model=new QSqlQueryModel;
    QString search,
            data;
    search = ui->searchArea->text();
    data = "select serialnumber as id, name as Name, author as Author, publication_date as publication_date, genre as Genre, amount_in_stock as Amount_in_stock from books where name LIKE '%"+search+"%'";
    model->setQuery(data);
    ui->tableView->setModel(model);
}

void findbookPage::connectData()
{
    db.connect();


    if(db.connect()==1) {
        sqlModel = new QSqlQueryModel();
        sqlModel->setQuery("select serialnumber as ID, name as Name, author as Author, publication_date as Publlication ,genre as Genre , amount_in_stock as Amount from books");
    }
    else {
        QMessageBox::information(this, "fail", "open failed");
    }
}

void findbookPage::refreshData()
{
    //remove row by row
    QAbstractItemModel *model = ui->tableView->model();
    int rowCount = model->rowCount();
    for(int r = rowCount - 1; r >= 0; r--) {
        model->removeRow(r);
    }

    appendData();

}

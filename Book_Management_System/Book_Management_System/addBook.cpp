#include "addBook.h"
#include "ui_addBook.h"

addBook::addBook(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addBook)
{
    ui->setupUi(this);
    this->setWindowTitle("Add new Book");
}

addBook::~addBook()
{
    delete ui;
}
void addBook::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPixmap pix;
    pix.load(":/images/rose2.png");
    painter.drawPixmap(0,0,pix);

}
void addBook::on_submitBtn_clicked()
{
    db.connect();

    getInput();
    //retrieve data from database
    if(db.connect()==1) {
        qDebug() <<"database connect succeed";

        getInput();
        QSqlQuery sql;
        QString checkExist = "select * from books where serialnumber = '"+_id+"' ";
        if(book_name.isEmpty() || book_author.isEmpty() || book_publication_date.isEmpty()
                || book_genre.isEmpty() || book_amount_in_stock.isEmpty() )
        {
            QMessageBox::about(this, "sign up failed", "Please fill in all the information");
        }

        else if(sql.exec(checkExist))
        {
            if(sql.next() == true)
            {
                QMessageBox::information(this, "Not Inserted", "This book already exist");
            }
            else if(sql.next() == false)
            {
                db.add_book(_id,book_name,book_author,book_publication_date,book_genre,book_amount_in_stock);
                QMessageBox::information(this, "Submited", "Submited successful");
                this->hide();
                clearInput();
                db.close();
            }
        }

    }else
    {
        QMessageBox::information(this, "Not connected", "database is not conntected");
    }


}

void addBook::getInput()
{
    book_name = ui->txt_BookName->text();
    book_author = ui->txt_owner->text();
    book_publication_date = ui->txt_created_date->text();
    book_genre = ui->txt_model->text();
    book_amount_in_stock =ui->txt_number_in_store->text();
    QUuid u=QUuid::createUuid();
    QString id=u.toString();
    _id = id;
}

void addBook::clearInput()
{
    ui->txt_BookName->clear();
    ui->txt_owner->clear();
    ui->txt_created_date->clear();
    ui->txt_model->clear();
    ui->txt_number_in_store->clear();

}

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = Book_management
TEMPLATE = app


DEFINES += QT_DEPRECATED_WARNINGS
# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    addBook.cpp \
    adminstrator.cpp \
    clickablelabel.cpp \
    edit_orders.cpp \
    findbookpage.cpp \
    main.cpp \
    mainscreen.cpp \
    mainwindow.cpp \
    reservebook.cpp \
    sign_in.cpp \
    updateprofile.cpp

HEADERS += \
    addBook.h \
    adminstrator.h \
    clickablelabel.h \
    databaseManager.h \
    edit_orders.h \
    emailaddress.h \
    findbookpage.h \
    mainscreen.h \
    mainwindow.h \
    reservebook.h \
    sign_in.h \
    updateprofile.h

FORMS += \
    addBook.ui \
    adminstrator.ui \
    edit_orders.ui \
    findbookpage.ui \
    mainscreen.ui \
    mainwindow.ui \
    reservebook.ui \
    sign_in.ui \
    updateprofile.ui

RESOURCES += \
    images.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

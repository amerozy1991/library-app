
#include "sign_in.h"
#include "ui_sign_in.h"
#include "mainwindow.h"

Sign_In::Sign_In(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Sign_In)
{
    ui->setupUi(this);
    this->setFixedSize(1100, 700);   //fix screen size
       this->setWindowTitle("Sign up");

       connect(ui->backLog, &clickableLabel::clicked, [=](){
           cleanLineEdit();
           emit this->backLogIn();
       });

}
void Sign_In::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPixmap pix;
    pix.load(":/images/CoverMain.png");
    painter.drawPixmap(0,0,pix);
}


void Sign_In::connectDatabase()
{
    db.connect();
}

void Sign_In::insertData()
{
    db.connect();
    getInput();

    QUuid u=QUuid::createUuid();
    QString id=u.toString();
    _id = id;
    QString Name = ui->fullNameBtn->text();
    QString Email = ui->emailBtn->text();
    QString Number = ui->phoneBtn->text();
    QString Password = ui->passwordBtn->text();
    QSqlQuery query= db.add_user(_id,Name,Email,Number,Password);
    if (query.exec())
    {
        QMessageBox::about(this, "Successful", "Sign Up successful, please back to Log in");
    }
    else {
        QMessageBox::about(this, "failed", "Sign Up failed, please check if connect database");
    }
}

void Sign_In::getInput()
{
    signName = ui->fullNameBtn->text();
    signPassword = ui->passwordBtn->text();
    signEmail = ui->emailBtn->text();
    signPhone = ui->phoneBtn->text();

}

void Sign_In::cleanLineEdit()
{
   ui->fullNameBtn->clear();
   ui->passwordBtn->clear();
   ui->emailBtn->clear();
   ui->phoneBtn->clear();

}



void Sign_In::on_signBtn_clicked()
{
    qDebug() << "connecting to Database";
        connectDatabase();
        if(db.connect()==1) {
            qDebug() <<"database connect succeed";

            getInput();
            QSqlQuery sql;
            QString checkExist = "select * from user where name = '"+signName+"' ";
            if(signName.isEmpty() || signPassword.isEmpty() ||
                    signEmail.isEmpty() || signPhone.isEmpty())
            {
                QMessageBox::about(this, "sign up failed", "Please Please fill in all the information");
            }
            else if(sql.exec(checkExist))
            {               
                if(sql.next() == true)
                {                  
                    QMessageBox::information(this, "Not Inserted", "This user name already exist");
                }
                else if(sql.next() == false)
                {
                    insertData();                
                    cleanLineEdit();
                    //db.close();
                }
            }

            }else {
            QMessageBox::information(this, "Not connected", "database is not conntected");
            }


}
Sign_In::~Sign_In()
{
    delete ui;
}

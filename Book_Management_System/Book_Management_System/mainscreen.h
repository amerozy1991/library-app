#ifndef MAINSCREEN_H
#define MAINSCREEN_H

#include <QWidget>
#include <QPainter>
#include <QSqlDatabase>
#include <QMessageBox>
#include <QSqlTableModel>
#include <QDebug>
#include <QSqlError>
#include <QSqlQueryModel>
#include "updateprofile.h"

#include "addBook.h"
#include "findbookpage.h"
#include "clickablelabel.h"
namespace Ui {
class MainScreen;
}

class MainScreen : public QWidget
{
    Q_OBJECT

public:
    explicit MainScreen(QWidget *parent = nullptr);
    ~MainScreen();

    void paintEvent(QPaintEvent *);

        //User name entered on the connection login page
        QString user_name, user_id;
        void init();
        void myOrders();

       updateProfile *updatePage;
        findbookPage* findBook;
       /* creatbook* createBookPage;*/



    signals:
        void backToLogIn();

    private slots:
        void on_editBtn_clicked();



private:
    Ui::MainScreen *ui;
    QSqlDatabase dataBase;
    QSqlTableModel *tableModel;
    QSqlQueryModel *sqlModel;
};

#endif // MAINSCREEN_H

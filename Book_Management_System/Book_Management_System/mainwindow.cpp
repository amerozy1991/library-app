#include "mainwindow.h"
#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(1100, 700);
    this->setWindowTitle("Log in");
    ui->userNameBtn->setFocus();  //set the cursor on user name area
    ui->logInSuccessful->setVisible(false);

    // operator new pages of sign in, main screen and adminstrator
    signIn = new Sign_In;
    mainPage = new MainScreen;
    admin = new adminstrator;

    /*
        * Connect to sign in page and back
        * The login page provides an interface
        * to back to login from the main and sign up pages
        *
        */
    connect(ui->SignInBtn, &clickableLabel::clicked, [=](){
        // When the user click sign up button
        // The sign up page shows
        // Log in page hide
        signIn->show();
        this->close();
    });

    connect(signIn, &Sign_In::backLogIn, [=](){
        cleanLineEdit();
        signIn->hide();
        this->show();
    });

    connect(mainPage, &MainScreen::backToLogIn, [=](){
        QTimer::singleShot(500, this, [=](){
            mainPage->hide();
            this->show();
        });
    });

    connect(admin, &adminstrator::backToLogIn, [=](){
        admin->hide();
        this->show();
    });
}
void MainWindow::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPixmap pix;
    pix.load(":/images/CoverMain.png");
    painter.drawPixmap(0,0,pix);

}

void MainWindow::on_LogBtn_clicked()
{
    connectDatabase();

    //get username and password from user input areas



    if(db.connect()==1) {
        matchDatabase();
    }else {
        QMessageBox::information(this, "database", "connection failed");
    }

    // if database close in there, then the data cannot match
}


void MainWindow::connectDatabase()
{
    db.connect();
}

void MainWindow::matchDatabase()
{

    QString userName = ui->userNameBtn->text();
    QString password = ui->passwordBtn->text();
    QSqlQuery query= db.login_user(userName,password);
    if(!query.exec())
    {
        QMessageBox::information(this, "failed", "faile to execute");
    }
    else
    {
        if(query.next()) {
            //next is to determine whether the matching result is in the database
            //matching from 0
            QString userNameFromDB = query.value(1).toString();
            QString passwordFromDB = query.value(4).toString();

            if((userNameFromDB == "admin" && passwordFromDB == "123") || (userNameFromDB == "amero" && passwordFromDB == "123") ) {
                qDebug() <<"Admin login";   //testing Admin
                adminShow();
            }
            else if(userNameFromDB == userName && passwordFromDB == password)
            {
                mainScreenShow();
            }
        }
        else
        {
            QMessageBox::information(this, "failed", "Log in failed, Please check your username or "
                                                     "password");
        }
    }
    db.close();
}

void MainWindow::adminShow()
{
    QTimer::singleShot(500, this, [=](){
        ui->logInSuccessful->setVisible(true);
        QTimer::singleShot(1000, this, [=](){
            admin->show();
            this->hide();
            ui->logInSuccessful->setVisible(false);
        });
    });
}

void MainWindow::mainScreenShow()
{
    QString userName = ui->userNameBtn->text();
    //Pause 0.5 second when login successed for better user experience
    QTimer::singleShot(500, this, [=](){
        ui->logInSuccessful->setVisible(true);
    });
    QTimer::singleShot(1500, this, [=](){
        // username of main page is inherited to here
        mainPage->user_name = userName;
        this->hide();
        mainPage->show();
        //declear in mainPage, implement in here.
        //Otherwise the obtained data will be covered
        mainPage->init();
        ui->logInSuccessful->setVisible(false);
        cleanLineEdit();
    });

}

void MainWindow::cleanLineEdit()
{
    ui->userNameBtn->clear();
    ui->passwordBtn->clear();

}


MainWindow::~MainWindow()
{
    delete ui;
}


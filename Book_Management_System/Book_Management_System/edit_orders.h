#ifndef EDIT_ORDERS_H
#define EDIT_ORDERS_H

#include <QDialog>
#include <QPainter>
#include <QtSql>
#include <QSqlTableModel>
#include <QPainter>
#include <QMessageBox>
#include <QDebug>
#include "databaseManager.h"

namespace Ui {
class edit_orders;
}

class edit_orders : public QDialog
{
    Q_OBJECT

public:
    explicit edit_orders(QWidget *parent = nullptr);
    ~edit_orders();

     void appendData();
     void deleteorder();
     void refreshData();
     QStringList getCurrentData();
     void paintEvent(QPaintEvent *);
     QString userMail;

private slots:
     void searchFunc();
     void on_DeleteBtn_clicked();
     void on_btn_checkorder_clicked();
     void on_pushButton_clicked();

private:
    Ui::edit_orders *ui;
    QSqlTableModel* tableModel;
    databaseManager db;
      QSqlQueryModel *sqlModel;

};

#endif // EDIT_ORDERS_H

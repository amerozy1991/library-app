#include "mainscreen.h"
#include "ui_mainscreen.h"

MainScreen::MainScreen(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainScreen)
{
    ui->setupUi(this);
    this->setFixedSize(1100, 700);   //fix screen size
        this->setWindowTitle("Main Page");

        updatePage = new updateProfile;
        findBook = new findbookPage;

       //Widget default by the second screen
        ui->stackedWidget->setCurrentIndex(1);
        //switch to my trip
        connect(ui->tripsBtn, &QPushButton::clicked, [=](){
            myOrders();
            ui->stackedWidget->setCurrentIndex(0);
        });
        connect(ui->backAccountBtn, &QPushButton::clicked, [=](){
            ui->stackedWidget->setCurrentIndex(1);
        });


        connect(ui->goBack, &clickableLabel::clicked, [=](){
            emit this->backToLogIn();
           //emit reminds it is signal
        });

        //go to reserve interface
        connect(ui->findBookBtn, &QPushButton::clicked, [=](){
            findBook->refreshData();
            findBook->inputName = user_name ;
            findBook->show();
            this->hide();
        });

        connect(findBook, &findbookPage::backToMainPage, [=](){
            findBook->hide();
            this->show();
        });

       /*createBookPage = new creatbook;
       connect(ui->creatRideBtn, &QPushButton::clicked, [=](){
           createBookPage->show();
       });*/

}
void MainScreen::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPixmap pix;
    pix.load(":/images/rose2.png");
    painter.drawPixmap(0,0,pix);
}

void MainScreen::init()
{
    databaseManager db;
    db.connect();
    QString temp2 = user_name;
    QSqlQuery sql_userId;
        sql_userId.prepare("SELECT user_id FROM user WHERE name = '"+ temp2 +"' ");
        sql_userId.exec();
        if (sql_userId.next())
        {
            user_id = sql_userId.value(0).toString();
    }
    //testing if get username from login class
    qDebug() <<user_name;
    QString sql_user;
//    To get the name, email, phone number separately, because the areas of table view are not together
    sql_user ="select name from user where name='"+user_name+"'";
    QSqlQueryModel *sqlModel = new QSqlQueryModel;
    sqlModel->setQuery(sql_user);\
    ui->showUserName->horizontalHeader()->setVisible(false);
    ui->showUserName->verticalHeader()->setVisible(false);
    ui->showUserName->setShowGrid(false);
    ui->showUserName->setModel(sqlModel);

    //for phone number
    QString sql_user2;
    sql_user2 ="select number as Phonenumber from user where name='"+user_name+"'";
    QSqlQueryModel *sqlModel2 = new QSqlQueryModel;
    sqlModel2->setQuery(sql_user2);
    ui->showPhoneNumber->horizontalHeader()->setVisible(false);
    ui->showPhoneNumber->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->showPhoneNumber->verticalHeader()->setVisible(false);
    ui->showPhoneNumber->setShowGrid(false);
    ui->showPhoneNumber->setModel(sqlModel2);

//    //for email
    QString sql_user3;
    sql_user3 ="select email as Email from user where name='"+user_name+"'";
    QSqlQueryModel *sqlModel3 = new QSqlQueryModel;
    sqlModel3->setQuery(sql_user3);\
    ui->showEmail->horizontalHeader()->setVisible(false);
    ui->showEmail->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->showEmail->verticalHeader()->setVisible(false);
    ui->showEmail->setShowGrid(false);
    ui->showEmail->setModel(sqlModel3);
}

void MainScreen::myOrders()
{  
    sqlModel = new QSqlQueryModel();
    sqlModel->setQuery("select order_number, user_id, name_book,backdate as token_date,back_Date as returndate from rentorder where user_id = '"+user_id+"'");
    ui->orderNumber->verticalHeader()->setVisible(false);   //hide header
    ui->orderNumber->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->orderNumber->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->orderNumber->setModel(sqlModel);
}

void MainScreen::on_editBtn_clicked()
{
   updatePage->show();
}
MainScreen::~MainScreen()
{
    delete ui;
}

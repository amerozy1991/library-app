#ifndef SIGN_IN_H
#define SIGN_IN_H

#include <QMessageBox>
#include <QString>
#include <QSqlQueryModel>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>
#include <QPainter>
#include "databaseManager.h"
namespace Ui {
class Sign_In;
}

class Sign_In : public QWidget
{
    Q_OBJECT

public:
    explicit Sign_In(QWidget *parent = nullptr);
    ~Sign_In();
    void paintEvent(QPaintEvent *);
       void cleanLineEdit();
       void connectDatabase();
       void insertData();

   signals:
       //signals no need to complement
       void backLogIn();
private slots:

    void getInput();

    void on_signBtn_clicked();

private:
    Ui::Sign_In *ui;
     databaseManager db;
     QString signName;
     QString signPassword;
     QString signEmail;
     QString signPhone;
     QString _id;
};

#endif // SIGN_IN_H

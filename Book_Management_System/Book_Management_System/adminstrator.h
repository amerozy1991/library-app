#ifndef ADMINSTRATOR_H
#define ADMINSTRATOR_H

#include <QWidget>
#include <QtSql>
#include <QSqlTableModel>
#include <QPainter>
#include <QMessageBox>
#include <QDebug>
#include "databaseManager.h"
#include "addBook.h"
#include "edit_orders.h"


namespace Ui {
class adminstrator;
}

class adminstrator : public QWidget
{
    Q_OBJECT

public:
    explicit adminstrator(QWidget *parent = nullptr);
    ~adminstrator();

    void appendData();
    void paintEvent(QPaintEvent *);
    void refreshData();
private slots:
    void search();

    void on_pushButton_clicked();
    /* void ChilkatSample();*/
signals:
    void backToLogIn();


private:
    Ui::adminstrator *ui;
    edit_orders* orders_edit;
    QSqlTableModel* tableModel;
    addBook* addBookPage;
    databaseManager db;
};

#endif // ADMINSTRATOR_H

#include "adminstrator.h"
#include "ui_adminstrator.h"



adminstrator::adminstrator(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::adminstrator)
{
    ui->setupUi(this);
    this->setFixedSize(1100, 700);
    this->setWindowTitle("Adminstrator");
    connect(ui->searchLine, SIGNAL(textChanged(QString)),this, SLOT(search()));
    db.connect();
    appendData();
    refreshData();



    addBookPage = new addBook;
    connect(ui->creatBookBtn, &QPushButton::clicked, [=](){
        addBookPage->show();});

    orders_edit = new edit_orders;
    connect(ui->ViewOrderBtn, &QPushButton::clicked, [=](){
        orders_edit->refreshData();
        orders_edit->show();});
}
void adminstrator::appendData()
{
    db.connect();
    tableModel = new QSqlTableModel;
    tableModel->setTable("user");
    tableModel->select();
    tableModel->removeColumn(0);
    ui->usersView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    ui->usersView->setModel(tableModel);

}

void adminstrator::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPixmap pix;
    pix.load(":/images/blue.png");
    painter.drawPixmap(0,0,pix);

}


void adminstrator::search()
{
    db.connect();
    QSqlQueryModel *model=new QSqlQueryModel;
    QString search,
            data;
    search = ui->searchLine->text();
    data = "select name as username ,password as password,email as email, number as Phonenumber from user where name LIKE '%"+search+"%'";
    model->setQuery(data);
    ui->usersView->setModel(model);

}
void adminstrator::on_pushButton_clicked()
{

    emit this->backToLogIn();
}

void adminstrator::refreshData()
{
    db.connect();
    //remove row by row
    QAbstractItemModel *model = ui->usersView->model();
    int rowCount = model->rowCount();
    for(int r = rowCount - 1; r >= 0; r--) {
        model->removeRow(r);
    }

    appendData();

}

adminstrator::~adminstrator()
{
    delete ui;
}


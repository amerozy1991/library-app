#ifndef MAINWINDOW_H
#define MAINWINDOW_H



#include "sign_in.h"
#include "mainscreen.h"
#include "adminstrator.h"
#include <QMainWindow>
#include <QString>
#include <QSqlQuery>
#include <QDebug>
#include <QTimer>
#include <QPainter>
#include <QMessageBox>
#include "databaseManager.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void paintEvent(QPaintEvent *);\
    void cleanLineEdit();
    void connectDatabase();
    void matchDatabase();
    void adminShow();
    void mainScreenShow();
       Sign_In* signIn;
       MainScreen* mainPage;
       adminstrator* admin;

private slots:
   void on_LogBtn_clicked();
private:
    Ui::MainWindow *ui;
    //QSqlDatabase db;
     databaseManager db;
};
#endif // MAINWINDOW_H

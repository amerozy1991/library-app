/********************************************************************************
** Form generated from reading UI file 'sign_in.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SIGN_IN_H
#define UI_SIGN_IN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <clickableLabel.h>

QT_BEGIN_NAMESPACE

class Ui_Sign_In
{
public:
    QWidget *signTitle;
    QGridLayout *gridLayout;
    QLabel *signTitle_2;
    QWidget *widget_2;
    QVBoxLayout *verticalLayout;
    QLabel *fulName;
    QLineEdit *fullNameBtn;
    QLabel *password;
    QLineEdit *passwordBtn;
    QLabel *email;
    QLineEdit *emailBtn;
    QLabel *phoneNumber;
    QLineEdit *phoneBtn;
    QWidget *widget_3;
    QGridLayout *gridLayout_3;
    clickableLabel *backLog;
    QFrame *frame_2;
    QGridLayout *gridLayout_4;
    QPushButton *signBtn;

    void setupUi(QWidget *Sign_In)
    {
        if (Sign_In->objectName().isEmpty())
            Sign_In->setObjectName(QString::fromUtf8("Sign_In"));
        Sign_In->setEnabled(true);
        Sign_In->resize(1100, 700);
        Sign_In->setMaximumSize(QSize(1100, 700));
        signTitle = new QWidget(Sign_In);
        signTitle->setObjectName(QString::fromUtf8("signTitle"));
        signTitle->setGeometry(QRect(60, 50, 467, 65));
        signTitle->setStyleSheet(QString::fromUtf8("* {\n"
"color: rgb(242, 239, 232);\n"
"}"));
        gridLayout = new QGridLayout(signTitle);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        signTitle_2 = new QLabel(signTitle);
        signTitle_2->setObjectName(QString::fromUtf8("signTitle_2"));
        QFont font;
        font.setPointSize(28);
        signTitle_2->setFont(font);

        gridLayout->addWidget(signTitle_2, 0, 0, 1, 1);

        widget_2 = new QWidget(Sign_In);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        widget_2->setGeometry(QRect(290, 140, 441, 331));
        QFont font1;
        font1.setPointSize(18);
        widget_2->setFont(font1);
        widget_2->setMouseTracking(false);
        widget_2->setAutoFillBackground(false);
        widget_2->setStyleSheet(QString::fromUtf8("* {\n"
"outline: 0px;\n"
"padding-top: 10px;\n"
"}\n"
"\n"
"QLabel {\n"
"color:rgba(234, 232, 236, 230);\n"
"font-size: 17px;\n"
"}\n"
"\n"
"QLineEdit {\n"
"background: rgba(255, 255, 255, 0);\n"
"border-radius: 20px;\n"
"list-style: none;\n"
"border-bottom:1px solid rgba(249, 247, 250, 204);\n"
"border-radius: 20px;\n"
"color: rgba(222, 220, 223, 240);\n"
"\n"
"}"));
        verticalLayout = new QVBoxLayout(widget_2);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        fulName = new QLabel(widget_2);
        fulName->setObjectName(QString::fromUtf8("fulName"));

        verticalLayout->addWidget(fulName);

        fullNameBtn = new QLineEdit(widget_2);
        fullNameBtn->setObjectName(QString::fromUtf8("fullNameBtn"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(fullNameBtn->sizePolicy().hasHeightForWidth());
        fullNameBtn->setSizePolicy(sizePolicy);
        fullNameBtn->setMinimumSize(QSize(400, 30));
        fullNameBtn->setFocusPolicy(Qt::StrongFocus);
        fullNameBtn->setMaxLength(25);

        verticalLayout->addWidget(fullNameBtn);

        password = new QLabel(widget_2);
        password->setObjectName(QString::fromUtf8("password"));

        verticalLayout->addWidget(password);

        passwordBtn = new QLineEdit(widget_2);
        passwordBtn->setObjectName(QString::fromUtf8("passwordBtn"));
        sizePolicy.setHeightForWidth(passwordBtn->sizePolicy().hasHeightForWidth());
        passwordBtn->setSizePolicy(sizePolicy);
        passwordBtn->setMinimumSize(QSize(400, 30));
        passwordBtn->setFocusPolicy(Qt::StrongFocus);
        passwordBtn->setMaxLength(25);
        passwordBtn->setEchoMode(QLineEdit::Password);

        verticalLayout->addWidget(passwordBtn);

        email = new QLabel(widget_2);
        email->setObjectName(QString::fromUtf8("email"));

        verticalLayout->addWidget(email);

        emailBtn = new QLineEdit(widget_2);
        emailBtn->setObjectName(QString::fromUtf8("emailBtn"));
        sizePolicy.setHeightForWidth(emailBtn->sizePolicy().hasHeightForWidth());
        emailBtn->setSizePolicy(sizePolicy);
        emailBtn->setMinimumSize(QSize(400, 30));
        emailBtn->setFocusPolicy(Qt::StrongFocus);
        emailBtn->setMaxLength(110);
        emailBtn->setEchoMode(QLineEdit::Normal);

        verticalLayout->addWidget(emailBtn);

        phoneNumber = new QLabel(widget_2);
        phoneNumber->setObjectName(QString::fromUtf8("phoneNumber"));

        verticalLayout->addWidget(phoneNumber);

        phoneBtn = new QLineEdit(widget_2);
        phoneBtn->setObjectName(QString::fromUtf8("phoneBtn"));
        sizePolicy.setHeightForWidth(phoneBtn->sizePolicy().hasHeightForWidth());
        phoneBtn->setSizePolicy(sizePolicy);
        phoneBtn->setMinimumSize(QSize(400, 30));
        phoneBtn->setFocusPolicy(Qt::StrongFocus);
        phoneBtn->setMaxLength(25);
        phoneBtn->setEchoMode(QLineEdit::Normal);

        verticalLayout->addWidget(phoneBtn);

        widget_3 = new QWidget(Sign_In);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        widget_3->setGeometry(QRect(570, 500, 161, 51));
        widget_3->setFont(font1);
        widget_3->setFocusPolicy(Qt::NoFocus);
        widget_3->setStyleSheet(QString::fromUtf8("QLabel {\n"
"color:rgba(238, 236, 239, 240);\n"
"text-decoration: underline;\n"
"font-size: 20px;\n"
"}\n"
"\n"
"QLabel::hover {\n"
"color: rgba(175, 203, 219, 240);\n"
"}"));
        gridLayout_3 = new QGridLayout(widget_3);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        backLog = new clickableLabel(widget_3);
        backLog->setObjectName(QString::fromUtf8("backLog"));
        QFont font2;
        font2.setUnderline(true);
        backLog->setFont(font2);
        backLog->setCursor(QCursor(Qt::PointingHandCursor));
        backLog->setFocusPolicy(Qt::StrongFocus);

        gridLayout_3->addWidget(backLog, 0, 0, 1, 1);

        frame_2 = new QFrame(Sign_In);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setGeometry(QRect(290, 490, 144, 64));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame_2->sizePolicy().hasHeightForWidth());
        frame_2->setSizePolicy(sizePolicy1);
        frame_2->setMinimumSize(QSize(100, 0));
        frame_2->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"border-radius: 20px;\n"
"background: rgba(233, 232, 235, 128);\n"
"color: rgba(254, 252, 255, 245);\n"
"}\n"
"\n"
"QPushButton::hover {\n"
"background-color:rgba(167, 193, 209, 204);\n"
"}"));
        frame_2->setFrameShape(QFrame::NoFrame);
        frame_2->setFrameShadow(QFrame::Raised);
        gridLayout_4 = new QGridLayout(frame_2);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        signBtn = new QPushButton(frame_2);
        signBtn->setObjectName(QString::fromUtf8("signBtn"));
        sizePolicy.setHeightForWidth(signBtn->sizePolicy().hasHeightForWidth());
        signBtn->setSizePolicy(sizePolicy);
        signBtn->setMinimumSize(QSize(120, 40));
        signBtn->setFont(font1);
        signBtn->setCursor(QCursor(Qt::PointingHandCursor));
        signBtn->setFocusPolicy(Qt::StrongFocus);

        gridLayout_4->addWidget(signBtn, 0, 0, 1, 1);

        QWidget::setTabOrder(fullNameBtn, passwordBtn);
        QWidget::setTabOrder(passwordBtn, emailBtn);
        QWidget::setTabOrder(emailBtn, phoneBtn);
        QWidget::setTabOrder(phoneBtn, backLog);
        QWidget::setTabOrder(backLog, widget_3);

        retranslateUi(Sign_In);

        QMetaObject::connectSlotsByName(Sign_In);
    } // setupUi

    void retranslateUi(QWidget *Sign_In)
    {
        Sign_In->setWindowTitle(QCoreApplication::translate("Sign_In", "Form", nullptr));
        signTitle_2->setText(QCoreApplication::translate("Sign_In", "Create a new account", nullptr));
        fulName->setText(QCoreApplication::translate("Sign_In", "Full Name", nullptr));
        password->setText(QCoreApplication::translate("Sign_In", "Password", nullptr));
        email->setText(QCoreApplication::translate("Sign_In", "E-mail", nullptr));
        phoneNumber->setText(QCoreApplication::translate("Sign_In", "Phone Number", nullptr));
        backLog->setText(QCoreApplication::translate("Sign_In", "Back to Log In ", nullptr));
        signBtn->setText(QCoreApplication::translate("Sign_In", "Sign in", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Sign_In: public Ui_Sign_In {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SIGN_IN_H

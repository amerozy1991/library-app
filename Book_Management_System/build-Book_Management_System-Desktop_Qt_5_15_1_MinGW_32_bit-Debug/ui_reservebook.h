/********************************************************************************
** Form generated from reading UI file 'reservebook.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RESERVEBOOK_H
#define UI_RESERVEBOOK_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>
#include <clickableLabel.h>

QT_BEGIN_NAMESPACE

class Ui_reservebook
{
public:
    QLabel *label_6;
    QWidget *widget_2;
    clickableLabel *confirmBtn;
    QWidget *widget_6;
    QLabel *txt_model;
    QLabel *txt_number_in_store;
    QLabel *label_4;
    QLabel *label_5;
    QWidget *widget_4;
    QLabel *txt_owner;
    QLabel *txt_created_Date;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *txtbookName;
    QWidget *widget_3;

    void setupUi(QDialog *reservebook)
    {
        if (reservebook->objectName().isEmpty())
            reservebook->setObjectName(QString::fromUtf8("reservebook"));
        reservebook->resize(500, 600);
        reservebook->setMaximumSize(QSize(1000, 700));
        label_6 = new QLabel(reservebook);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(0, 10, 500, 141));
        label_6->setPixmap(QPixmap(QString::fromUtf8(":/images/books_wide.png")));
        label_6->setScaledContents(true);
        widget_2 = new QWidget(reservebook);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        widget_2->setGeometry(QRect(50, 220, 16, 151));
        widget_2->setStyleSheet(QString::fromUtf8("QWidget {\n"
"border-right: 2px solid rgb(177, 176, 178);\n"
"}"));
        confirmBtn = new clickableLabel(reservebook);
        confirmBtn->setObjectName(QString::fromUtf8("confirmBtn"));
        confirmBtn->setGeometry(QRect(170, 530, 161, 41));
        confirmBtn->setCursor(QCursor(Qt::PointingHandCursor));
        confirmBtn->setStyleSheet(QString::fromUtf8("QLabel {\n"
"font-size: 17px;\n"
"color: rgb(91, 104, 127);\n"
"text-decoration: underline;\n"
"}"));
        widget_6 = new QWidget(reservebook);
        widget_6->setObjectName(QString::fromUtf8("widget_6"));
        widget_6->setGeometry(QRect(70, 370, 371, 151));
        widget_6->setStyleSheet(QString::fromUtf8("QLabel {\n"
"width: 200px;\n"
"height: 30px;\n"
"font-size: 15px;\n"
"color: rgb(91, 104, 127);\n"
"}"));
        txt_model = new QLabel(widget_6);
        txt_model->setObjectName(QString::fromUtf8("txt_model"));
        txt_model->setGeometry(QRect(0, 30, 351, 41));
        txt_model->setMinimumSize(QSize(0, 30));
        txt_number_in_store = new QLabel(widget_6);
        txt_number_in_store->setObjectName(QString::fromUtf8("txt_number_in_store"));
        txt_number_in_store->setGeometry(QRect(0, 100, 351, 41));
        txt_number_in_store->setMinimumSize(QSize(0, 30));
        label_4 = new QLabel(widget_6);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(0, 10, 61, 16));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label_4->setFont(font);
        label_5 = new QLabel(widget_6);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(0, 80, 141, 16));
        label_5->setFont(font);
        widget_4 = new QWidget(reservebook);
        widget_4->setObjectName(QString::fromUtf8("widget_4"));
        widget_4->setGeometry(QRect(70, 220, 371, 151));
        widget_4->setStyleSheet(QString::fromUtf8("QLabel {\n"
"width: 150px;\n"
"height: 30px;\n"
"font-size: 14px;\n"
"color: rgb(91, 104, 127);\n"
"}"));
        txt_owner = new QLabel(widget_4);
        txt_owner->setObjectName(QString::fromUtf8("txt_owner"));
        txt_owner->setGeometry(QRect(0, 30, 361, 41));
        txt_owner->setMinimumSize(QSize(0, 30));
        txt_created_Date = new QLabel(widget_4);
        txt_created_Date->setObjectName(QString::fromUtf8("txt_created_Date"));
        txt_created_Date->setGeometry(QRect(0, 100, 361, 41));
        txt_created_Date->setMinimumSize(QSize(0, 30));
        label_2 = new QLabel(widget_4);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(0, 10, 61, 16));
        label_2->setFont(font);
        label_3 = new QLabel(widget_4);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(0, 80, 91, 16));
        label_3->setFont(font);
        txtbookName = new QLabel(reservebook);
        txtbookName->setObjectName(QString::fromUtf8("txtbookName"));
        txtbookName->setGeometry(QRect(10, 160, 481, 51));
        QFont font1;
        font1.setPointSize(18);
        txtbookName->setFont(font1);
        txtbookName->setLayoutDirection(Qt::LeftToRight);
        txtbookName->setStyleSheet(QString::fromUtf8("QLabel {\n"
"color: rgb(91, 104, 127);\n"
"}"));
        txtbookName->setAlignment(Qt::AlignCenter);
        widget_3 = new QWidget(reservebook);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        widget_3->setGeometry(QRect(50, 370, 16, 151));
        widget_3->setStyleSheet(QString::fromUtf8("QWidget {\n"
"border-right: 2px solid rgb(177, 176, 178);\n"
"}"));

        retranslateUi(reservebook);

        QMetaObject::connectSlotsByName(reservebook);
    } // setupUi

    void retranslateUi(QDialog *reservebook)
    {
        reservebook->setWindowTitle(QCoreApplication::translate("reservebook", "Dialog", nullptr));
        label_6->setText(QString());
        confirmBtn->setText(QCoreApplication::translate("reservebook", "Confirm Reservation", nullptr));
        txt_model->setText(QCoreApplication::translate("reservebook", "text", nullptr));
        txt_number_in_store->setText(QCoreApplication::translate("reservebook", "text", nullptr));
        label_4->setText(QCoreApplication::translate("reservebook", "Genre :", nullptr));
        label_5->setText(QCoreApplication::translate("reservebook", "available Amount :", nullptr));
        txt_owner->setText(QCoreApplication::translate("reservebook", "text", nullptr));
        txt_created_Date->setText(QCoreApplication::translate("reservebook", "text", nullptr));
        label_2->setText(QCoreApplication::translate("reservebook", "Author : ", nullptr));
        label_3->setText(QCoreApplication::translate("reservebook", "Published in :", nullptr));
        txtbookName->setText(QCoreApplication::translate("reservebook", "TextLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class reservebook: public Ui_reservebook {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RESERVEBOOK_H

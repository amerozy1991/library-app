/********************************************************************************
** Form generated from reading UI file 'mainscreen.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINSCREEN_H
#define UI_MAINSCREEN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QWidget>
#include <clickableLabel.h>

QT_BEGIN_NAMESPACE

class Ui_MainScreen
{
public:
    clickableLabel *goBack;
    QWidget *widget_3;
    QLabel *label_2;
    QPushButton *findBookBtn;
    QWidget *widget;
    QStackedWidget *stackedWidget;
    QWidget *page1;
    QPushButton *backAccountBtn;
    QLabel *label_6;
    QTableView *orderNumber;
    QWidget *page2;
    QPushButton *tripsBtn;
    QPushButton *editBtn;
    QWidget *profile;
    QLabel *label;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_7;
    QTableView *showEmail;
    QTableView *showPhoneNumber;
    QTableView *showUserName;

    void setupUi(QWidget *MainScreen)
    {
        if (MainScreen->objectName().isEmpty())
            MainScreen->setObjectName(QString::fromUtf8("MainScreen"));
        MainScreen->resize(1100, 700);
        MainScreen->setMaximumSize(QSize(1100, 700));
        QFont font;
        font.setPointSize(9);
        MainScreen->setFont(font);
        goBack = new clickableLabel(MainScreen);
        goBack->setObjectName(QString::fromUtf8("goBack"));
        goBack->setGeometry(QRect(900, 620, 81, 31));
        QFont font1;
        font1.setBold(false);
        font1.setItalic(false);
        font1.setUnderline(true);
        font1.setWeight(50);
        goBack->setFont(font1);
        goBack->setCursor(QCursor(Qt::PointingHandCursor));
        goBack->setStyleSheet(QString::fromUtf8("QLabel {\n"
"font: 18px;\n"
"color: rgb(241, 242, 241);\n"
"text-decoration: underline;\n"
"}\n"
"\n"
"QLabel::hover {\n"
"color: rgba(242, 242, 220, 153);\n"
"\n"
"}"));
        widget_3 = new QWidget(MainScreen);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        widget_3->setGeometry(QRect(690, 160, 331, 271));
        widget_3->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"background: rgba(229, 232, 255, 40);\n"
"color: white;\n"
"border-radius: 20px;\n"
"}\n"
"\n"
"QPushButton::hover {\n"
"background: rgba(232, 216, 255, 128);\n"
"\n"
"}\n"
""));
        label_2 = new QLabel(widget_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(70, 20, 191, 161));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/images/Books.png")));
        label_2->setScaledContents(true);
        findBookBtn = new QPushButton(widget_3);
        findBookBtn->setObjectName(QString::fromUtf8("findBookBtn"));
        findBookBtn->setGeometry(QRect(80, 210, 161, 51));
        QFont font2;
        font2.setPointSize(18);
        findBookBtn->setFont(font2);
        findBookBtn->setCursor(QCursor(Qt::PointingHandCursor));
        widget = new QWidget(MainScreen);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(60, 20, 581, 651));
        stackedWidget = new QStackedWidget(widget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setGeometry(QRect(0, 20, 571, 571));
        stackedWidget->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"background: rgba(237, 215, 214, 80);\n"
"color: white;\n"
"border-radius: 20px;\n"
"font-size: 18px;\n"
"width: 170px;\n"
"height: 55px;\n"
"}\n"
"\n"
"QPushButton::hover {\n"
"background: rgba(229, 232, 255, 40);\n"
"}\n"
"\n"
"QStackedWidget{\n"
"border: 3px solid rgba(105, 105, 100, 70);\n"
"border-radius: 20px;\n"
"}\n"
""));
        stackedWidget->setFrameShape(QFrame::NoFrame);
        page1 = new QWidget();
        page1->setObjectName(QString::fromUtf8("page1"));
        QFont font3;
        font3.setPointSize(1);
        page1->setFont(font3);
        backAccountBtn = new QPushButton(page1);
        backAccountBtn->setObjectName(QString::fromUtf8("backAccountBtn"));
        backAccountBtn->setGeometry(QRect(320, 490, 144, 44));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(backAccountBtn->sizePolicy().hasHeightForWidth());
        backAccountBtn->setSizePolicy(sizePolicy);
        backAccountBtn->setMinimumSize(QSize(144, 44));
        backAccountBtn->setCursor(QCursor(Qt::PointingHandCursor));
        label_6 = new QLabel(page1);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(140, 10, 241, 51));
        label_6->setStyleSheet(QString::fromUtf8("QLabel {\n"
"font-size: 28px;\n"
"color: rgb(92, 89, 106);\n"
"}"));
        orderNumber = new QTableView(page1);
        orderNumber->setObjectName(QString::fromUtf8("orderNumber"));
        orderNumber->setGeometry(QRect(20, 90, 531, 385));
        orderNumber->setMaximumSize(QSize(560, 385));
        orderNumber->setStyleSheet(QString::fromUtf8("QTableView {\n"
"background: rgba(174, 174, 174, 10);\n"
"border-radius: 15px;\n"
"font-size: 10px;\n"
"color: rgb(244, 244, 244);\n"
"}"));
        stackedWidget->addWidget(page1);
        page2 = new QWidget();
        page2->setObjectName(QString::fromUtf8("page2"));
        sizePolicy.setHeightForWidth(page2->sizePolicy().hasHeightForWidth());
        page2->setSizePolicy(sizePolicy);
        page2->setMinimumSize(QSize(170, 55));
        tripsBtn = new QPushButton(page2);
        tripsBtn->setObjectName(QString::fromUtf8("tripsBtn"));
        tripsBtn->setGeometry(QRect(270, 470, 141, 41));
        tripsBtn->setCursor(QCursor(Qt::PointingHandCursor));
        editBtn = new QPushButton(page2);
        editBtn->setObjectName(QString::fromUtf8("editBtn"));
        editBtn->setGeometry(QRect(60, 470, 141, 41));
        editBtn->setCursor(QCursor(Qt::PointingHandCursor));
        profile = new QWidget(page2);
        profile->setObjectName(QString::fromUtf8("profile"));
        profile->setGeometry(QRect(30, 40, 431, 401));
        profile->setStyleSheet(QString::fromUtf8("QTableView{\n"
"background: rgba(251, 239, 220, 15);\n"
"border-radius: 15px;\n"
"color: rgb(80, 77, 89);\n"
"selection-background-color: rgba(245, 245, 245, 0);\n"
"selection-color:  rgb(80, 77, 89);\n"
"}\n"
"\n"
"QWidget {\n"
"}\n"
"\n"
"QLabel {\n"
"color: rgb(92, 89, 106);\n"
"}"));
        label = new QLabel(profile);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(170, 30, 81, 51));
        label->setFont(font2);
        label_4 = new QLabel(profile);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(30, 190, 131, 31));
        QFont font4;
        font4.setPointSize(15);
        label_4->setFont(font4);
        label_5 = new QLabel(profile);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(30, 300, 141, 31));
        label_5->setFont(font4);
        label_7 = new QLabel(profile);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(40, 10, 101, 91));
        label_7->setFont(font4);
        label_7->setPixmap(QPixmap(QString::fromUtf8(":/images/profile.png")));
        label_7->setScaledContents(true);
        showEmail = new QTableView(profile);
        showEmail->setObjectName(QString::fromUtf8("showEmail"));
        showEmail->setGeometry(QRect(30, 230, 331, 31));
        QFont font5;
        font5.setPointSize(20);
        showEmail->setFont(font5);
        showEmail->setFocusPolicy(Qt::NoFocus);
        showEmail->setContextMenuPolicy(Qt::NoContextMenu);
        showPhoneNumber = new QTableView(profile);
        showPhoneNumber->setObjectName(QString::fromUtf8("showPhoneNumber"));
        showPhoneNumber->setGeometry(QRect(30, 340, 331, 31));
        QFont font6;
        font6.setPointSize(20);
        font6.setBold(false);
        font6.setItalic(false);
        font6.setWeight(50);
        showPhoneNumber->setFont(font6);
        showPhoneNumber->setFocusPolicy(Qt::NoFocus);
        showPhoneNumber->setContextMenuPolicy(Qt::NoContextMenu);
        showUserName = new QTableView(profile);
        showUserName->setObjectName(QString::fromUtf8("showUserName"));
        showUserName->setGeometry(QRect(260, 40, 161, 31));
        QFont font7;
        font7.setPointSize(18);
        font7.setBold(false);
        font7.setItalic(false);
        font7.setWeight(50);
        showUserName->setFont(font7);
        showUserName->setFocusPolicy(Qt::NoFocus);
        showUserName->setContextMenuPolicy(Qt::NoContextMenu);
        showUserName->setStyleSheet(QString::fromUtf8("QTableView {\n"
"color: rgb(80, 77, 89);\n"
"}"));
        stackedWidget->addWidget(page2);

        retranslateUi(MainScreen);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainScreen);
    } // setupUi

    void retranslateUi(QWidget *MainScreen)
    {
        MainScreen->setWindowTitle(QCoreApplication::translate("MainScreen", "Form", nullptr));
        goBack->setText(QCoreApplication::translate("MainScreen", "Sign Out", nullptr));
        label_2->setText(QString());
        findBookBtn->setText(QCoreApplication::translate("MainScreen", "find a book", nullptr));
        backAccountBtn->setText(QCoreApplication::translate("MainScreen", "Back", nullptr));
        label_6->setText(QCoreApplication::translate("MainScreen", "Your ordered books:", nullptr));
        tripsBtn->setText(QCoreApplication::translate("MainScreen", "My books", nullptr));
        editBtn->setText(QCoreApplication::translate("MainScreen", "Edit Account", nullptr));
        label->setText(QCoreApplication::translate("MainScreen", "  Hello", nullptr));
        label_4->setText(QCoreApplication::translate("MainScreen", "Email:", nullptr));
        label_5->setText(QCoreApplication::translate("MainScreen", "Phone Number:", nullptr));
        label_7->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainScreen: public Ui_MainScreen {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINSCREEN_H

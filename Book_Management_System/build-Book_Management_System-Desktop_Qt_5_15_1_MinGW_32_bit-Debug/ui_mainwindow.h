/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
#include <clickableLabel.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QFrame *LogMain;
    QGridLayout *gridLayout;
    QLabel *userName;
    QLineEdit *userNameBtn;
    QLabel *Password;
    QLineEdit *passwordBtn;
    QSpacerItem *verticalSpacer;
    QLabel *logInSuccessful;
    QFrame *frame_3;
    QGridLayout *gridLayout_4;
    QLabel *NoAccount;
    clickableLabel *SignInBtn;
    QFrame *frame;
    QGridLayout *gridLayout_2;
    QLabel *title;
    QFrame *frame_2;
    QGridLayout *gridLayout_3;
    QPushButton *LogBtn;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->setEnabled(true);
        MainWindow->resize(1100, 700);
        MainWindow->setMaximumSize(QSize(1100, 700));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        LogMain = new QFrame(centralwidget);
        LogMain->setObjectName(QString::fromUtf8("LogMain"));
        LogMain->setGeometry(QRect(140, 250, 561, 141));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(LogMain->sizePolicy().hasHeightForWidth());
        LogMain->setSizePolicy(sizePolicy);
        LogMain->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"width: 150px;\n"
"height: 30px;\n"
"border-radius: 20px;\n"
"background: rgba(233, 232, 235, 128);\n"
"list-style: none;\n"
"}\n"
"\n"
"QLabel {\n"
"color: rgb(232, 229, 223);\n"
"}"));
        LogMain->setFrameShape(QFrame::NoFrame);
        LogMain->setFrameShadow(QFrame::Raised);
        gridLayout = new QGridLayout(LogMain);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        userName = new QLabel(LogMain);
        userName->setObjectName(QString::fromUtf8("userName"));
        sizePolicy.setHeightForWidth(userName->sizePolicy().hasHeightForWidth());
        userName->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(18);
        userName->setFont(font);
        userName->setFrameShape(QFrame::NoFrame);

        gridLayout->addWidget(userName, 0, 0, 1, 1);

        userNameBtn = new QLineEdit(LogMain);
        userNameBtn->setObjectName(QString::fromUtf8("userNameBtn"));
        sizePolicy.setHeightForWidth(userNameBtn->sizePolicy().hasHeightForWidth());
        userNameBtn->setSizePolicy(sizePolicy);
        userNameBtn->setMinimumSize(QSize(300, 40));
        userNameBtn->setMaximumSize(QSize(300, 40));
        QFont font1;
        font1.setPointSize(16);
        userNameBtn->setFont(font1);
        userNameBtn->setCursor(QCursor(Qt::ArrowCursor));
        userNameBtn->setMaxLength(25);
        userNameBtn->setEchoMode(QLineEdit::Normal);

        gridLayout->addWidget(userNameBtn, 0, 1, 1, 1);

        Password = new QLabel(LogMain);
        Password->setObjectName(QString::fromUtf8("Password"));
        sizePolicy.setHeightForWidth(Password->sizePolicy().hasHeightForWidth());
        Password->setSizePolicy(sizePolicy);
        Password->setFont(font);

        gridLayout->addWidget(Password, 2, 0, 1, 1);

        passwordBtn = new QLineEdit(LogMain);
        passwordBtn->setObjectName(QString::fromUtf8("passwordBtn"));
        sizePolicy.setHeightForWidth(passwordBtn->sizePolicy().hasHeightForWidth());
        passwordBtn->setSizePolicy(sizePolicy);
        passwordBtn->setMinimumSize(QSize(300, 40));
        passwordBtn->setMaximumSize(QSize(300, 40));
        passwordBtn->setFont(font1);
        passwordBtn->setCursor(QCursor(Qt::ArrowCursor));
        passwordBtn->setMaxLength(25);
        passwordBtn->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(passwordBtn, 2, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 1, 1, 1, 1);

        logInSuccessful = new QLabel(centralwidget);
        logInSuccessful->setObjectName(QString::fromUtf8("logInSuccessful"));
        logInSuccessful->setGeometry(QRect(190, 180, 171, 51));
        logInSuccessful->setStyleSheet(QString::fromUtf8("QLabel {\n"
"font: 20px;\n"
"color: white;\n"
"}"));
        frame_3 = new QFrame(centralwidget);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setGeometry(QRect(850, 530, 126, 76));
        frame_3->setStyleSheet(QString::fromUtf8("QLabel {\n"
"font-size: 18px;\n"
"color: white;\n"
"}"));
        frame_3->setFrameShape(QFrame::NoFrame);
        frame_3->setFrameShadow(QFrame::Raised);
        gridLayout_4 = new QGridLayout(frame_3);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        NoAccount = new QLabel(frame_3);
        NoAccount->setObjectName(QString::fromUtf8("NoAccount"));
        QFont font2;
        NoAccount->setFont(font2);

        gridLayout_4->addWidget(NoAccount, 0, 0, 1, 1);

        SignInBtn = new clickableLabel(frame_3);
        SignInBtn->setObjectName(QString::fromUtf8("SignInBtn"));
        SignInBtn->setMaximumSize(QSize(108, 26));
        QFont font3;
        font3.setUnderline(true);
        SignInBtn->setFont(font3);
        SignInBtn->setCursor(QCursor(Qt::PointingHandCursor));
        SignInBtn->setFocusPolicy(Qt::StrongFocus);
        SignInBtn->setStyleSheet(QString::fromUtf8("QLabel {\n"
"text-decoration: underline;\n"
"\n"
"}\n"
"\n"
"\n"
"QLabel::hover {\n"
"color: rgba(229, 227, 219, 198);\n"
"\n"
"}"));

        gridLayout_4->addWidget(SignInBtn, 1, 0, 1, 1);

        frame = new QFrame(centralwidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setGeometry(QRect(120, 10, 531, 91));
        frame->setStyleSheet(QString::fromUtf8("* {\n"
"color: rgba(245, 243, 246, 243);\n"
"}"));
        frame->setFrameShape(QFrame::NoFrame);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout_2 = new QGridLayout(frame);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        title = new QLabel(frame);
        title->setObjectName(QString::fromUtf8("title"));
        QFont font4;
        font4.setPointSize(28);
        font4.setBold(true);
        font4.setWeight(75);
        title->setFont(font4);

        gridLayout_2->addWidget(title, 1, 0, 1, 1);

        frame_2 = new QFrame(centralwidget);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setGeometry(QRect(430, 440, 144, 64));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame_2->sizePolicy().hasHeightForWidth());
        frame_2->setSizePolicy(sizePolicy1);
        frame_2->setMinimumSize(QSize(100, 0));
        frame_2->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"border-radius: 20px;\n"
"background: rgba(233, 232, 235, 128);\n"
"color: rgba(254, 252, 255, 245);\n"
"}\n"
"\n"
"QPushButton::hover {\n"
"background-color:rgba(167, 193, 209, 204);\n"
"}"));
        frame_2->setFrameShape(QFrame::NoFrame);
        frame_2->setFrameShadow(QFrame::Raised);
        gridLayout_3 = new QGridLayout(frame_2);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        LogBtn = new QPushButton(frame_2);
        LogBtn->setObjectName(QString::fromUtf8("LogBtn"));
        sizePolicy.setHeightForWidth(LogBtn->sizePolicy().hasHeightForWidth());
        LogBtn->setSizePolicy(sizePolicy);
        LogBtn->setMinimumSize(QSize(120, 40));
        LogBtn->setFont(font);
        LogBtn->setCursor(QCursor(Qt::PointingHandCursor));
        LogBtn->setFocusPolicy(Qt::StrongFocus);

        gridLayout_3->addWidget(LogBtn, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralwidget);
        QWidget::setTabOrder(userNameBtn, passwordBtn);
        QWidget::setTabOrder(passwordBtn, LogBtn);
        QWidget::setTabOrder(LogBtn, SignInBtn);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        userName->setText(QCoreApplication::translate("MainWindow", "User name", nullptr));
        Password->setText(QCoreApplication::translate("MainWindow", "Password", nullptr));
        logInSuccessful->setText(QCoreApplication::translate("MainWindow", "Log In Successful", nullptr));
        NoAccount->setText(QCoreApplication::translate("MainWindow", "No account?", nullptr));
        SignInBtn->setText(QCoreApplication::translate("MainWindow", "Sign up here", nullptr));
        title->setText(QCoreApplication::translate("MainWindow", "Book Management System", nullptr));
        LogBtn->setText(QCoreApplication::translate("MainWindow", "Log In", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H

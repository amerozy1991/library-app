/********************************************************************************
** Form generated from reading UI file 'edit_orders.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_ORDERS_H
#define UI_EDIT_ORDERS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_edit_orders
{
public:
    QVBoxLayout *verticalLayout;
    QLineEdit *searchArea;
    QTableView *tableView;
    QPushButton *DeleteBtn;
    QPushButton *btn_checkorder;
    QPushButton *pushButton;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *edit_orders)
    {
        if (edit_orders->objectName().isEmpty())
            edit_orders->setObjectName(QString::fromUtf8("edit_orders"));
        edit_orders->resize(542, 392);
        verticalLayout = new QVBoxLayout(edit_orders);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        searchArea = new QLineEdit(edit_orders);
        searchArea->setObjectName(QString::fromUtf8("searchArea"));
        searchArea->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"background: rgb(116, 120, 140, 0);\n"
"border: 1px solid rgb(63, 65, 77);\n"
"border-radius: 15px;\n"
"\n"
"}"));

        verticalLayout->addWidget(searchArea);

        tableView = new QTableView(edit_orders);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setStyleSheet(QString::fromUtf8("QTableView {\n"
"border: 1px solid rgba(118, 122, 142, 153);\n"
"background: rgba(224, 222, 225, 51);\n"
"selection-background-color: rgba(136, 132, 159, 204);\n"
"selection-color: rgb(248, 246, 250);\n"
"}\n"
"\n"
"QHeaderView::section {\n"
"border-radius: 30px;\n"
"height: 30px;\n"
"color: rgb(239, 238, 241);\n"
"background: rgb(108, 104, 124);\n"
"}\n"
""));

        verticalLayout->addWidget(tableView);

        DeleteBtn = new QPushButton(edit_orders);
        DeleteBtn->setObjectName(QString::fromUtf8("DeleteBtn"));
        DeleteBtn->setEnabled(true);
        DeleteBtn->setCursor(QCursor(Qt::PointingHandCursor));
        DeleteBtn->setFocusPolicy(Qt::TabFocus);
        DeleteBtn->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"width: 518px;\n"
"height: 32px;\n"
"background:  rgba(208, 205, 228, 100);\n"
"color:rgb(255, 255, 255);\n"
"border-radius: 10px;\n"
"}\n"
"\n"
"QPushButton::hover {\n"
"background: rgba(208, 205, 228, 153);\n"
"}"));

        verticalLayout->addWidget(DeleteBtn);

        btn_checkorder = new QPushButton(edit_orders);
        btn_checkorder->setObjectName(QString::fromUtf8("btn_checkorder"));
        btn_checkorder->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"width: 518px;\n"
"height: 32px;\n"
"background:  rgba(208, 205, 228, 100);\n"
"color:rgb(255, 255, 255);\n"
"border-radius: 10px;\n"
"}\n"
"\n"
"QPushButton::hover {\n"
"background: rgba(208, 205, 228, 153);\n"
"}"));

        verticalLayout->addWidget(btn_checkorder);

        pushButton = new QPushButton(edit_orders);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout->addWidget(pushButton);

        buttonBox = new QDialogButtonBox(edit_orders);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(edit_orders);
        QObject::connect(buttonBox, SIGNAL(accepted()), edit_orders, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), edit_orders, SLOT(reject()));

        QMetaObject::connectSlotsByName(edit_orders);
    } // setupUi

    void retranslateUi(QDialog *edit_orders)
    {
        edit_orders->setWindowTitle(QCoreApplication::translate("edit_orders", "Dialog", nullptr));
        DeleteBtn->setText(QCoreApplication::translate("edit_orders", "delete", nullptr));
        btn_checkorder->setText(QCoreApplication::translate("edit_orders", "checkorder", nullptr));
        pushButton->setText(QCoreApplication::translate("edit_orders", "PushButton", nullptr));
    } // retranslateUi

};

namespace Ui {
    class edit_orders: public Ui_edit_orders {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_ORDERS_H

/********************************************************************************
** Form generated from reading UI file 'adminstrator.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADMINSTRATOR_H
#define UI_ADMINSTRATOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_adminstrator
{
public:
    QLineEdit *searchLine;
    QTableView *usersView;
    QLabel *label;
    QPushButton *pushButton;
    QLabel *label_3;
    QPushButton *creatBookBtn;
    QLabel *label_4;
    QPushButton *ViewOrderBtn;
    QLabel *label_5;
    QPushButton *deleteBook;

    void setupUi(QWidget *adminstrator)
    {
        if (adminstrator->objectName().isEmpty())
            adminstrator->setObjectName(QString::fromUtf8("adminstrator"));
        adminstrator->resize(1100, 700);
        adminstrator->setMaximumSize(QSize(1100, 700));
        searchLine = new QLineEdit(adminstrator);
        searchLine->setObjectName(QString::fromUtf8("searchLine"));
        searchLine->setGeometry(QRect(90, 70, 201, 31));
        searchLine->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"background: rgb(116, 120, 140, 0);\n"
"border: 1px solid rgb(63, 65, 77);\n"
"border-radius: 15px;\n"
"\n"
"}"));
        usersView = new QTableView(adminstrator);
        usersView->setObjectName(QString::fromUtf8("usersView"));
        usersView->setGeometry(QRect(50, 120, 751, 401));
        usersView->setStyleSheet(QString::fromUtf8("QTableView {\n"
"border: 1px solid rgba(118, 122, 142, 153);\n"
"background: rgba(224, 222, 225, 51);\n"
"selection-background-color: rgba(136, 132, 159, 204);\n"
"selection-color: rgb(248, 246, 250);\n"
"}\n"
"\n"
"QHeaderView::section {\n"
"border-radius: 30px;\n"
"height: 30px;\n"
"color: rgb(239, 238, 241);\n"
"background: rgb(108, 104, 124);\n"
"}\n"
""));
        label = new QLabel(adminstrator);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(60, 80, 21, 21));
        label->setPixmap(QPixmap(QString::fromUtf8(":/images/search.png")));
        label->setScaledContents(true);
        pushButton = new QPushButton(adminstrator);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(680, 540, 121, 41));
        pushButton->setMinimumSize(QSize(0, 0));
        pushButton->setMaximumSize(QSize(16777215, 16777215));
        pushButton->setCursor(QCursor(Qt::PointingHandCursor));
        pushButton->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"background: rgba(124, 128, 149, 179);\n"
"border-radius: 20px;\n"
"font-size: 16px;\n"
"color: white;\n"
"}\n"
"\n"
"QPushButton::hover {\n"
"background: rgba(124, 128, 149, 100);\n"
"}"));
        label_3 = new QLabel(adminstrator);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(830, 120, 100, 100));
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/images/Add_New_Book.png")));
        label_3->setScaledContents(true);
        creatBookBtn = new QPushButton(adminstrator);
        creatBookBtn->setObjectName(QString::fromUtf8("creatBookBtn"));
        creatBookBtn->setGeometry(QRect(970, 150, 115, 40));
        QFont font;
        font.setPointSize(11);
        creatBookBtn->setFont(font);
        creatBookBtn->setCursor(QCursor(Qt::PointingHandCursor));
        creatBookBtn->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"width: 100px;\n"
"height: 35px;\n"
"background:  rgba(208, 205, 228, 100);\n"
"color:rgb(255, 255, 255);\n"
"border-radius: 10px;\n"
"}\n"
"\n"
"QPushButton::hover {\n"
"background: rgba(208, 205, 228, 153);\n"
"}"));
        label_4 = new QLabel(adminstrator);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(830, 400, 100, 100));
        QFont font1;
        font1.setPointSize(10);
        label_4->setFont(font1);
        label_4->setPixmap(QPixmap(QString::fromUtf8(":/images/Orders.png")));
        label_4->setScaledContents(true);
        ViewOrderBtn = new QPushButton(adminstrator);
        ViewOrderBtn->setObjectName(QString::fromUtf8("ViewOrderBtn"));
        ViewOrderBtn->setGeometry(QRect(970, 430, 115, 40));
        ViewOrderBtn->setFont(font);
        ViewOrderBtn->setCursor(QCursor(Qt::PointingHandCursor));
        ViewOrderBtn->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"width: 100px;\n"
"height: 35px;\n"
"background:  rgba(208, 205, 228, 100);\n"
"color:rgb(255, 255, 255);\n"
"border-radius: 10px;\n"
"}\n"
"\n"
"QPushButton::hover {\n"
"background: rgba(208, 205, 228, 153);\n"
"}"));
        label_5 = new QLabel(adminstrator);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(830, 260, 100, 100));
        QFont font2;
        font2.setPointSize(5);
        label_5->setFont(font2);
        label_5->setPixmap(QPixmap(QString::fromUtf8(":/images/delete_book.png")));
        label_5->setScaledContents(true);
        deleteBook = new QPushButton(adminstrator);
        deleteBook->setObjectName(QString::fromUtf8("deleteBook"));
        deleteBook->setGeometry(QRect(970, 290, 115, 40));
        deleteBook->setFont(font);
        deleteBook->setCursor(QCursor(Qt::PointingHandCursor));
        deleteBook->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"width: 100px;\n"
"height: 35px;\n"
"background:  rgba(208, 205, 228, 100);\n"
"color:rgb(255, 255, 255);\n"
"border-radius: 10px;\n"
"}\n"
"\n"
"QPushButton::hover {\n"
"background: rgba(208, 205, 228, 153);\n"
"}"));

        retranslateUi(adminstrator);

        QMetaObject::connectSlotsByName(adminstrator);
    } // setupUi

    void retranslateUi(QWidget *adminstrator)
    {
        adminstrator->setWindowTitle(QCoreApplication::translate("adminstrator", "Form", nullptr));
        label->setText(QString());
        pushButton->setText(QCoreApplication::translate("adminstrator", "Back to Log In", nullptr));
        label_3->setText(QString());
        creatBookBtn->setText(QCoreApplication::translate("adminstrator", "Add a Book", nullptr));
        label_4->setText(QString());
        ViewOrderBtn->setText(QCoreApplication::translate("adminstrator", "View ordes", nullptr));
        label_5->setText(QString());
        deleteBook->setText(QCoreApplication::translate("adminstrator", "Remove a Book", nullptr));
    } // retranslateUi

};

namespace Ui {
    class adminstrator: public Ui_adminstrator {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADMINSTRATOR_H

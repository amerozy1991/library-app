/********************************************************************************
** Form generated from reading UI file 'updateprofile.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UPDATEPROFILE_H
#define UI_UPDATEPROFILE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_updateProfile
{
public:
    QWidget *widget_2;
    QGridLayout *gridLayout_2;
    QPushButton *cancelBtn;
    QPushButton *updateBtn;
    QSpacerItem *horizontalSpacer;
    QWidget *widget;
    QGridLayout *gridLayout;
    QLabel *label;
    QLineEdit *currentlName;
    QLabel *label_2;
    QLineEdit *newPhoneNumber;
    QLabel *label_3;
    QLineEdit *newEmail;
    QLabel *label_4;
    QLineEdit *newPassword;

    void setupUi(QDialog *updateProfile)
    {
        if (updateProfile->objectName().isEmpty())
            updateProfile->setObjectName(QString::fromUtf8("updateProfile"));
        updateProfile->resize(311, 371);
        widget_2 = new QWidget(updateProfile);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        widget_2->setGeometry(QRect(10, 300, 291, 59));
        widget_2->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"width: 80px;\n"
"height: 35px;\n"
"background: rgba(255, 224, 201, 83);\n"
"color:rgb(255, 255, 255);\n"
"border-radius: 10px;\n"
"font: 15px;\n"
"}\n"
"\n"
"QPushButton::hover {\n"
"background:  rgba(255, 224, 201, 143);\n"
"}"));
        gridLayout_2 = new QGridLayout(widget_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        cancelBtn = new QPushButton(widget_2);
        cancelBtn->setObjectName(QString::fromUtf8("cancelBtn"));
        cancelBtn->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_2->addWidget(cancelBtn, 0, 2, 1, 1);

        updateBtn = new QPushButton(widget_2);
        updateBtn->setObjectName(QString::fromUtf8("updateBtn"));
        updateBtn->setCursor(QCursor(Qt::PointingHandCursor));
        updateBtn->setFocusPolicy(Qt::TabFocus);
        updateBtn->setStyleSheet(QString::fromUtf8(""));

        gridLayout_2->addWidget(updateBtn, 0, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer, 0, 1, 1, 1);

        widget = new QWidget(updateProfile);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(10, 10, 291, 271));
        widget->setStyleSheet(QString::fromUtf8("QLabel {\n"
"font: 16px;\n"
"color: rgb(86, 83, 103);\n"
"}\n"
"\n"
"QLineEdit {\n"
"width: 200px;\n"
"height: 15px;\n"
"color: rgb(86, 83, 103);\n"
"font: 16px;\n"
"background: rgba(226, 219, 245, 0);\n"
"border-bottom: 1px solid  rgb(86, 83, 103);\n"
"}"));
        gridLayout = new QGridLayout(widget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(widget);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        currentlName = new QLineEdit(widget);
        currentlName->setObjectName(QString::fromUtf8("currentlName"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(currentlName->sizePolicy().hasHeightForWidth());
        currentlName->setSizePolicy(sizePolicy);
        currentlName->setMinimumSize(QSize(0, 15));
        currentlName->setFocusPolicy(Qt::ClickFocus);
        currentlName->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"height: 30px;\n"
"border-radius: 10px;\n"
"margin-top: 10px;\n"
"}"));

        gridLayout->addWidget(currentlName, 1, 0, 1, 1);

        label_2 = new QLabel(widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 2, 0, 1, 1);

        newPhoneNumber = new QLineEdit(widget);
        newPhoneNumber->setObjectName(QString::fromUtf8("newPhoneNumber"));
        newPhoneNumber->setMinimumSize(QSize(0, 15));
        newPhoneNumber->setFocusPolicy(Qt::ClickFocus);
        newPhoneNumber->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"height: 30px;\n"
"border-radius: 10px;\n"
"margin-top: 10px;\n"
"}"));

        gridLayout->addWidget(newPhoneNumber, 3, 0, 1, 1);

        label_3 = new QLabel(widget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 4, 0, 1, 1);

        newEmail = new QLineEdit(widget);
        newEmail->setObjectName(QString::fromUtf8("newEmail"));
        newEmail->setMinimumSize(QSize(0, 15));
        newEmail->setFocusPolicy(Qt::ClickFocus);
        newEmail->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"height: 30px;\n"
"border-radius: 10px;\n"
"margin-top: 10px;\n"
"}"));

        gridLayout->addWidget(newEmail, 5, 0, 1, 1);

        label_4 = new QLabel(widget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 6, 0, 1, 1);

        newPassword = new QLineEdit(widget);
        newPassword->setObjectName(QString::fromUtf8("newPassword"));
        newPassword->setMinimumSize(QSize(0, 15));
        newPassword->setFocusPolicy(Qt::ClickFocus);
        newPassword->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"height: 30px;\n"
"border-radius: 10px;\n"
"margin-top: 10px;\n"
"}"));

        gridLayout->addWidget(newPassword, 7, 0, 1, 1);


        retranslateUi(updateProfile);

        QMetaObject::connectSlotsByName(updateProfile);
    } // setupUi

    void retranslateUi(QDialog *updateProfile)
    {
        updateProfile->setWindowTitle(QCoreApplication::translate("updateProfile", "Dialog", nullptr));
        cancelBtn->setText(QCoreApplication::translate("updateProfile", "Cancel", nullptr));
        updateBtn->setText(QCoreApplication::translate("updateProfile", "Update", nullptr));
        label->setText(QCoreApplication::translate("updateProfile", "Current Name:", nullptr));
        currentlName->setText(QString());
        label_2->setText(QCoreApplication::translate("updateProfile", "New Phone Number:", nullptr));
        label_3->setText(QCoreApplication::translate("updateProfile", "New E-mail:", nullptr));
        label_4->setText(QCoreApplication::translate("updateProfile", "New Password:", nullptr));
        newPassword->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class updateProfile: public Ui_updateProfile {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UPDATEPROFILE_H

/********************************************************************************
** Form generated from reading UI file 'findbookpage.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FINDBOOKPAGE_H
#define UI_FINDBOOKPAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_findbookPage
{
public:
    QLabel *label_2;
    QLabel *label;
    QPushButton *reserveBtn;
    QTableView *tableView;
    QPushButton *backBtn;
    QLineEdit *searchArea;

    void setupUi(QWidget *findbookPage)
    {
        if (findbookPage->objectName().isEmpty())
            findbookPage->setObjectName(QString::fromUtf8("findbookPage"));
        findbookPage->resize(1100, 700);
        findbookPage->setMaximumSize(QSize(1100, 700));
        label_2 = new QLabel(findbookPage);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(50, 0, 991, 180));
        label_2->setMaximumSize(QSize(991, 180));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/images/books_wide.png")));
        label_2->setScaledContents(true);
        label = new QLabel(findbookPage);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(670, 190, 41, 41));
        label->setMaximumSize(QSize(50, 50));
        label->setPixmap(QPixmap(QString::fromUtf8(":/images/search.png")));
        label->setScaledContents(true);
        reserveBtn = new QPushButton(findbookPage);
        reserveBtn->setObjectName(QString::fromUtf8("reserveBtn"));
        reserveBtn->setGeometry(QRect(910, 190, 90, 40));
        reserveBtn->setMaximumSize(QSize(90, 40));
        reserveBtn->setCursor(QCursor(Qt::PointingHandCursor));
        reserveBtn->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"background: rgba(154, 154, 154, 178);\n"
"font-size: 16px;\n"
"border-radius: 15px;\n"
"}\n"
"\n"
"\n"
"QPushButton::hover{\n"
"background: rgba(154, 154, 154, 100);\n"
"}"));
        tableView = new QTableView(findbookPage);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setGeometry(QRect(80, 250, 911, 410));
        tableView->setMaximumSize(QSize(911, 410));
        tableView->setStyleSheet(QString::fromUtf8("QTableView {\n"
"border-radius: 30px;\n"
"background: rgba(224, 222, 225, 51);\n"
"selection-background-color: rgba(143, 141, 161, 204);\n"
"selection-color: rgb(243, 241, 244);\n"
"}\n"
"\n"
"QHeaderView::section {\n"
"height: 30px;\n"
"background: rgb(240, 238, 241);\n"
"border-radius: 20px;\n"
"}\n"
""));
        tableView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        tableView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        tableView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        backBtn = new QPushButton(findbookPage);
        backBtn->setObjectName(QString::fromUtf8("backBtn"));
        backBtn->setGeometry(QRect(90, 190, 90, 40));
        backBtn->setMaximumSize(QSize(90, 40));
        backBtn->setCursor(QCursor(Qt::PointingHandCursor));
        backBtn->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"background: rgba(154, 154, 154, 178);\n"
"font-size: 16px;\n"
"border-radius: 15px;\n"
"}\n"
"\n"
"\n"
"QPushButton::hover{\n"
"background: rgba(154, 154, 154, 100);\n"
"}"));
        searchArea = new QLineEdit(findbookPage);
        searchArea->setObjectName(QString::fromUtf8("searchArea"));
        searchArea->setGeometry(QRect(370, 190, 291, 41));
        searchArea->setMaximumSize(QSize(320, 50));
        searchArea->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"background: rgba(200, 200, 200, 150);\n"
"border-radius: 15px;\n"
"color: black;\n"
"font-size: 15px;\n"
"}\n"
""));

        retranslateUi(findbookPage);

        QMetaObject::connectSlotsByName(findbookPage);
    } // setupUi

    void retranslateUi(QWidget *findbookPage)
    {
        findbookPage->setWindowTitle(QCoreApplication::translate("findbookPage", "Form", nullptr));
        label_2->setText(QString());
        label->setText(QString());
        reserveBtn->setText(QCoreApplication::translate("findbookPage", "Reserve", nullptr));
        backBtn->setText(QCoreApplication::translate("findbookPage", "Back ", nullptr));
    } // retranslateUi

};

namespace Ui {
    class findbookPage: public Ui_findbookPage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FINDBOOKPAGE_H

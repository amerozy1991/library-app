/********************************************************************************
** Form generated from reading UI file 'addBook.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDBOOK_H
#define UI_ADDBOOK_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_addBook
{
public:
    QWidget *widget;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QLabel *label_3;
    QLineEdit *txt_model;
    QLabel *label_4;
    QLabel *label_5;
    QLineEdit *txt_created_date;
    QLineEdit *txt_owner;
    QLineEdit *txt_number_in_store;
    QLabel *name;
    QLineEdit *txt_BookName;
    QPushButton *submitBtn;

    void setupUi(QDialog *addBook)
    {
        if (addBook->objectName().isEmpty())
            addBook->setObjectName(QString::fromUtf8("addBook"));
        addBook->setEnabled(true);
        addBook->resize(351, 482);
        widget = new QWidget(addBook);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(10, 10, 331, 401));
        widget->setStyleSheet(QString::fromUtf8("QLabel {\n"
"font: 16px;\n"
"color: rgb(86, 83, 103);\n"
"}\n"
"\n"
"QLineEdit {\n"
"width: 200px;\n"
"height: 15px;\n"
"font:16px;\n"
"color: rgb(86, 83, 103);\n"
"background: rgba(226, 219, 245, 0);\n"
"border-bottom: 1px solid  rgb(86, 83, 103);\n"
"}\n"
"\n"
""));
        gridLayout = new QGridLayout(widget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_2 = new QLabel(widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 3, 0, 1, 1);

        label_3 = new QLabel(widget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 5, 0, 1, 1);

        txt_model = new QLineEdit(widget);
        txt_model->setObjectName(QString::fromUtf8("txt_model"));
        txt_model->setMinimumSize(QSize(0, 15));
        txt_model->setFocusPolicy(Qt::ClickFocus);
        txt_model->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"height: 30px;\n"
"border-radius: 10px;\n"
"margin-top: 10px;\n"
"}"));

        gridLayout->addWidget(txt_model, 8, 0, 1, 1);

        label_4 = new QLabel(widget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 7, 0, 1, 1);

        label_5 = new QLabel(widget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 9, 0, 1, 1);

        txt_created_date = new QLineEdit(widget);
        txt_created_date->setObjectName(QString::fromUtf8("txt_created_date"));
        txt_created_date->setMinimumSize(QSize(0, 15));
        txt_created_date->setFocusPolicy(Qt::ClickFocus);
        txt_created_date->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"height: 30px;\n"
"border-radius: 10px;\n"
"margin-top: 10px;\n"
"}"));

        gridLayout->addWidget(txt_created_date, 6, 0, 1, 1);

        txt_owner = new QLineEdit(widget);
        txt_owner->setObjectName(QString::fromUtf8("txt_owner"));
        txt_owner->setMinimumSize(QSize(0, 15));
        txt_owner->setFocusPolicy(Qt::ClickFocus);
        txt_owner->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"height: 30px;\n"
"border-radius: 10px;\n"
"margin-top: 10px;\n"
"}"));

        gridLayout->addWidget(txt_owner, 4, 0, 1, 1);

        txt_number_in_store = new QLineEdit(widget);
        txt_number_in_store->setObjectName(QString::fromUtf8("txt_number_in_store"));
        txt_number_in_store->setMinimumSize(QSize(0, 15));
        txt_number_in_store->setFocusPolicy(Qt::ClickFocus);
        txt_number_in_store->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"height: 30px;\n"
"border-radius: 10px;\n"
"margin-top: 10px;\n"
"}"));

        gridLayout->addWidget(txt_number_in_store, 10, 0, 1, 1);

        name = new QLabel(widget);
        name->setObjectName(QString::fromUtf8("name"));

        gridLayout->addWidget(name, 0, 0, 1, 1);

        txt_BookName = new QLineEdit(widget);
        txt_BookName->setObjectName(QString::fromUtf8("txt_BookName"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(txt_BookName->sizePolicy().hasHeightForWidth());
        txt_BookName->setSizePolicy(sizePolicy);
        txt_BookName->setMinimumSize(QSize(0, 15));
        txt_BookName->setFocusPolicy(Qt::ClickFocus);
        txt_BookName->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"height: 30px;\n"
"border-radius: 10px;\n"
"margin-top: 10px;\n"
"}"));

        gridLayout->addWidget(txt_BookName, 2, 0, 1, 1);

        submitBtn = new QPushButton(addBook);
        submitBtn->setObjectName(QString::fromUtf8("submitBtn"));
        submitBtn->setGeometry(QRect(10, 420, 111, 41));
        QFont font;
        font.setPointSize(16);
        submitBtn->setFont(font);
        submitBtn->setCursor(QCursor(Qt::PointingHandCursor));
        submitBtn->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"width: 100px;\n"
"height: 35px;\n"
"background:  rgba(208, 205, 228, 100);\n"
"color:rgb(255, 255, 255);\n"
"border-radius: 10px;\n"
"}\n"
"\n"
"QPushButton::hover {\n"
"background: rgba(208, 205, 228, 153);\n"
"}"));

        retranslateUi(addBook);

        QMetaObject::connectSlotsByName(addBook);
    } // setupUi

    void retranslateUi(QDialog *addBook)
    {
        addBook->setWindowTitle(QCoreApplication::translate("addBook", "Dialog", nullptr));
        label_2->setText(QCoreApplication::translate("addBook", "Author	", nullptr));
        label_3->setText(QCoreApplication::translate("addBook", "Publication date", nullptr));
        label_4->setText(QCoreApplication::translate("addBook", "Genre", nullptr));
        label_5->setText(QCoreApplication::translate("addBook", "amount in stock", nullptr));
        name->setText(QCoreApplication::translate("addBook", "Book Name", nullptr));
        txt_BookName->setText(QString());
        submitBtn->setText(QCoreApplication::translate("addBook", "Submit", nullptr));
    } // retranslateUi

};

namespace Ui {
    class addBook: public Ui_addBook {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDBOOK_H
